<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Alert;

class PenerimaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penerima = User::all();
        return view('penerima.index', compact('penerima'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penerima.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $penerima = User::create([
            "name" => $request["name"],
            "gender" => $request["gender"],
            "email" => $request["email"],
            "alamat" => $request["alamat"],
            'password' => Hash::make($request['password']),
        ]);
        Alert::success('Berhasil', 'Data penerima bantuan berhasil disimpan');
        return redirect('/penerima');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penerima = User::find($id);
        return view('penerima.show', compact('penerima'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penerima = User::find($id);
        return view('penerima.edit', compact('penerima'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = User::where('id', $id)->update([
            "name" => $request["name"],
            "gender" => $request["gender"],
            "email" => $request["email"],
            "alamat" => $request["alamat"],
            ]);
            Alert::success('Berhasil', 'Data penerima bantuan diperbarui');
            return redirect('/penerima');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Alert::success('Berhasil', 'Data penerima bantuan dihapus');
        return redirect('/penerima');
    }
}
