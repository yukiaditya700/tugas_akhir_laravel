@extends('sbadmin2.master')

@section('content')
    <p>Nama: {{$penerima->name}}</p>
    <p>Email: {{$penerima->email}}</p>
    <p>Alamat: {{$penerima->alamat}}</p>
    <p>Jenis kelamin: {{$penerima->gender}}</p><br><br>
@if(Auth::user()->id == 1)
<a href="/penerima" class="btn btn-info">Kembali ke daftar penerima bantuan</a>
@else
<a href="/" class="btn btn-info">Kembali ke halaman utama</a>
@endif
@endsection