@extends('sbadmin2.master')

@section('content')
      <h3>Tambah penerima bantuan baru</h3>
    <form role="form" action="/penerima" method="POST">
    @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Nama</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ old('name','') }}" placeholder="Nama penerima bantuan">
          @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Email</label>
          <input type="text" class="form-control" id="email" name="email" value="{{ old('email','') }}" placeholder="Email penerima bantuan">
          @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Alamat</label>
          <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat','') }}" placeholder="Alamat penerima bantuan">
          @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="gender">Jenis Kelamin</label>
          <select class="form-control" name="gender">
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
        </select> 
            @error('gender')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="isi">Password</label>
          <input type="password" class="form-control" id="password" name="password" value="{{ old('password','') }}" placeholder="Password penerima bantuan">
          @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div>
        <button type="submit" class="btn btn-success">Masukkan</button>
        <a href="/penerima" class="btn btn-info">Kembali ke daftar penerima bantuan</a>
    </form>
@endsection