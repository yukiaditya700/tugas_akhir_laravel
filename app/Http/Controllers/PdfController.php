<?php

namespace App\Http\Controllers;
use App\User;
use PDF;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function pdf() {
        $data = User::get();
        $pdf = PDF::loadView('pdf', compact('data'));
        return $pdf->download('penerimabantuan.pdf');
    }
}
