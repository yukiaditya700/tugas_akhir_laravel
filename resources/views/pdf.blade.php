<table>
  <thead>
    <tr>
      <th>Nama</th>
      <th>Gender</th>
      <th>Email</th>
      <th>Alamat</th>
    </tr>
  </thead>
  <tbody>
@foreach($data as $value)
    <tr>
        <td>{{ $value->name }}</td>
        <td>{{ $value->gender }}</td>
        <td>{{ $value->email }}</td>
        <td>{{ $value->alamat }}</td>
    </tr>
@endforeach
    </tbody>
</table>