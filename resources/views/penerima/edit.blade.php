@extends('sbadmin2.master')

@section('content')
      <h3>Edit data penerima bantuan</h3>
      <form role="form" action="/penerima/{{$penerima->id}}" method="POST">
    <div class="card-body">
    @csrf
    @method('PUT')
        <div class="form-group">
          <label for="judul">Nama</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $penerima->name) }}" placeholder="Masukan Nama Siswa">
          @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Email</label>
          <input type="text" class="form-control" id="email" name="email" value="{{ old('email',$penerima->email) }}" placeholder="email">
          @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Alamat</label>
          <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat',$penerima->alamat) }}" placeholder="alamat">
          @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="gender">Jenis Kelamin</label>
          <select class="form-control" name="gender">
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
        </select> 
            @error('gender')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
        <button type="submit" class="btn btn-success">Perbarui</button>
        <a href="/penerima" class="btn btn-info">Kembali ke daftar penerima bantuan</a>
      </div>
    </form>
@endsection