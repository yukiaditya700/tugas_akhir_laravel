@extends('sbadmin2.master')

@section('content')
              <h2>Data penerima bantuan Gempa Mamuju 2021</h2>
                <a class="btn btn-primary mb-2" href="/penerima/create">Tambah penerima bantuan baru</a>
              <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama lengkap</th>
                    <th scope="col">Jenis kelamin</th>
                    <th scope="col"">Action</th>
                  </tr>
                </thead>
                <tbody>
                @forelse($penerima as $key => $value)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->gender}}</td>
                        <td style="display:flex;">
                            <a href="/penerima/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/penerima/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/penerima/{{$value->id}}" method="post">   
                                @csrf    
                                @method('delete')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="4">Tidak ada data</td>
                </tr>
                @endforelse
                </tbody>
              </table>
@endsection