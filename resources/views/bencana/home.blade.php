@extends('sbadmin2.master')

@section('content')
    <h1>Selamat datang {{Auth::user()->name}}</h1>                
    @if(Auth::user()->id == 1)
    <h2>Cek semua informasi penerima bantuan bencana Mamuju di link bawah ini</h2>
    <a href="/penerima">lihat informasi penerima bantuan bencana</a>
    @else
    <h2>Data anda telah tercatat di database penerima bantuan bencana Mamuju</h2>
    <a href="/penerima/{{Auth::user()->id}}">lihat data informasi anda</a>
    @endif
@endsection
