<?php

namespace App\Http\Controllers;

use App\Exports\ListExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ExcelController extends Controller
{
    public function export() 
    {
        return Excel::download(new ListExport, 'penerimabantuan.xlsx');
    }
}
